import {SET_JWT, SET_USER} from '../actions/authentification';
import {SET_CONTACTS, UPDATE_CONTACT, APPEND_CONTACT, REMOVE_CONTACT} from '../actions/contacts';

const initialValues = {
    JWT: undefined,
    contacts: [],
    user: undefined,
};

function reducers(state = initialValues, action) {
    switch (action.type) {
        case SET_JWT:
            return {...state, JWT: action.JWT};
        case SET_USER:
            return {...state, user: action.user};
        case SET_CONTACTS:
            return {...state, contacts: action.contacts};
        case UPDATE_CONTACT:
            return {
                ...state,
                contacts: state.contacts.map((value) => {
                    return value.id === action.contact.id ? action.contact : value;
                }),
            };
        case APPEND_CONTACT:
            return {
                ...state,
                contacts: [...state.contacts, action.contact],
            };
        case REMOVE_CONTACT:
            return {
                ...state,
                contacts: state.contacts.filter((value) => {
                    return value.id !== action.contact.id;
                }),
            };
        default:
            return state;
    }
}

export default reducers;
