import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const buttonStyle = {
    marginBottom: 2, 
    backgroundColor: '#DDDDDD',
    alignItems: 'center', 
    padding: 10
  };

export const contactStyle = {
  backgroundColor: '#DDDDDD',
  margin: 10,
  width: wp('80%'),
  borderRadius: 10
};
