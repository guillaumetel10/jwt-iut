export const inputStyle = {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius:30,
    borderBottomWidth: 1,
    width:250,
    height:45,
    marginBottom:20,
    textAlign: 'center',
  };

export const buttonStyle = {
  borderBottomColor: '#F5FCFF',
  backgroundColor: '#57b846',
  borderRadius:30,
  borderBottomWidth: 1,
  width:250,
  height:45,
  marginBottom:20,
  justifyContent: 'center'
};

export const loginStyle = {
  color: 'white', 
  textAlign: 'center'
};
