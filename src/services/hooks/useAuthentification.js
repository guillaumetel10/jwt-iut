import React, {useContext, useState} from 'react';
import authentificationRequest from '../fetch/authentification';
import dispatchContext from '../store';
import {setJWT, setUser} from '../../actions/authentification';
import * as Keychain from 'react-native-keychain';

export function useAuthentification() {
    const {state, dispatch} = useContext(dispatchContext);
    const [erreur, setErreur] = useState('');

    function authenticate(connexion) {
        return authentificationRequest()
            .authenticate(connexion)
                .then((data) => {
                    if (data.error) {
                        console.log(data.error);
                        setErreur('Erreur: ' + data.error);
                    } else {
                        dispatch(setJWT(data.jwt));
                        Keychain.setGenericPassword(connexion.login, data.refreshToken);
                        setErreur('');
                        return data.jwt;
                        }
                })
                .catch((erreur) => {
                    setErreur('Erreur: ' + erreur);
                });
    }

    async function disconnect() {
        await Keychain.resetGenericPassword();
        dispatch(setUser(undefined));
        dispatch(setJWT(undefined));
    }

    return {
        authenticate : authenticate, 
        disconnect : disconnect,
        erreur : erreur 
    };
}

export default useAuthentification;
