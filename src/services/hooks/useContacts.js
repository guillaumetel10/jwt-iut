import {useState, useEffect, useContext} from 'react';
import contactsRequest from '../fetch/contacts';
import {setContacts} from '../../actions/contacts';
import dispatchContext from '../store';
import useRefreshToken from './useRefreshToken';

export function useContacts() {
    const [erreur, setErreur] = useState('');
    const {state, dispatch} = useContext(dispatchContext);
    const {getAllContacts} = contactsRequest(state.JWT);

    const JWTExpired = useRefreshToken();

    useEffect(() => {
        JWTExpired(getAllContacts)
            .then(function (data) {
                dispatch(setContacts(data));
                setErreur('');
            })
            .catch(function (erreur) {
                console.log('erreur: ' + erreur);
                setErreur('Erreur: ' + erreur.message);
            });
    }, []);

    return {erreur};
}

export default useContacts;
