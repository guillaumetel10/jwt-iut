import {useContext} from 'react';
import contactRequest from '../fetch/contacts';
import dispatchContext from '../store';
import {setJWT, setUser} from '../../actions/authentification';
import * as Keychain from 'react-native-keychain';
import useDecodeJWT from './useDecodeJWT';

export function useRegister() {
    const {state, dispatch} = useContext(dispatchContext);
    const {getId} = useDecodeJWT();
    const id = getId(state.JWT);

    function register(name, password) {
        return contactRequest(state.JWT)
            .register({id: id, name: name})
            .then((data) => {
                if (data.error) {
                    console.log(data.error);
                } else {
                    setUser(name)
                }
                return data;
            })
            .catch((erreur) => {
                console.log(erreur);
            });
    }

    return {register};
}

export default useRegister;
