import * as Keychain from 'react-native-keychain';
import useAuthentification from './useAuthentification';

export function useRefreshtoken() {
    const {authenticateRT} = useAuthentification();

    async function testJWTExpired(response, fonction) {
        if (response.status === 401) {
            const rep = await response.json();
            if (rep.code === 'tokenexpired') {
                return (async () => {
                    try {
                        const credentials = await Keychain.getGenericPassword();
                        if (credentials.password) {
                            return authenticateRT(credentials.username, credentials.password)
                                .then(() => {
                                    return fonction();
                                })
                                .catch((erreur) => {
                                    console.log(erreur);
                                });
                        }
                    } catch (error) {
                        console.log('Aucun Refresh Token', error);
                    }
                })();
            }
        } else if (response.status !== 204) {
            return response.json();
        }
    }
    function JWTExpired(fonction) {
        return fonction().then((response) => testJWTExpired(response, fonction));
    }
    return JWTExpired;
}

export default useRefreshtoken;
