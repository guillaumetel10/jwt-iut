import {useContext, useState, useEffect} from 'react';
import contactsRequest from '../fetch/contacts';
import dispatchContext from '../store';
import useRefreshToken from './useRefreshToken';

export function useContact(contact) {
    const {state} = useContext(dispatchContext);
    const {getContact} = contactsRequest(state.JWT);
    const [erreur, setErreur] = useState('');

    var [contactDetail, setContactDetail] = useState();

    const JWTExpired = useRefreshToken();

    useEffect(() => {
        JWTExpired(() => getContact(contact.id))
            .then(function (data) {
                setContactDetail(data);
                setErreur('');
            })
            .catch(function (erreur) {
                console.log('Erreur:', erreur);
                setErreur('Erreur: ' + erreur);
            });
    }, []);

    return {contactDetail, erreur};
}

export default useContact;
