import {useContext} from 'react';
import contactRequest from '../fetch/contacts';
import dispatchContext from '../store';

export function useDecodeJWT() {
    const {state, dispatch} = useContext(dispatchContext);

    const JWS = require('jsrsasign').jws.JWS;

    function decodeToken(jwt) {
        let payload = JWS.parse(jwt).payloadObj;
        return payload ? { userId: payload.sub } : {};
        }

    function getId(jwt) {
        return decodeToken(jwt).userId;
    }

    return {getId};
}

export default useDecodeJWT;
