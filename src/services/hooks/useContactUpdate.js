import {useContext} from 'react';
import contactsRequest from '../fetch/contacts';
import dispatchContext from '../store';
import useRefreshToken from './useRefreshToken';
import {useNavigation} from '@react-navigation/native';
import {updateContactReduce, appendContactReduce} from '../../actions/contacts';

export function useContactUpdate() {
    const navigation = useNavigation();
    const {state, dispatch} = useContext(dispatchContext);

    const {updateContact, appendContact} = contactsRequest(state.JWT);

    const JWTExpired = useRefreshToken();

    function enregistrerContact(contact) {
        JWTExpired(() => {
            return contact.id === undefined ? appendContact(contact) : updateContact(contact);
        })
            .then(function (data) {
                contact.id === undefined ? dispatch(appendContactReduce(data)) : dispatch(updateContactReduce(contact));
                navigation.navigate('ContactView', {contact: data});
            })
            .catch(function (erreur) {
                console.log('erreur: ' + erreur);
            });
    }
    return {enregistrerContact};
}

export default useContactUpdate;
