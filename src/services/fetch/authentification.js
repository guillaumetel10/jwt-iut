export function authentificationRequest() {
    const DATA_URL = 'http://localhost:8000/auth';

    function credential(options = {}) {
        return {
            ...options,
            credentials: 'include',
        };
    }

    function method(method, options = {}) {
        return {
            ...options,
            method,
        };
    }

    function body(data, options = {}) {
        return {
            ...options,
            body: JSON.stringify(data),
        };
    }

    function mimeType(mimeType, options = {}) {
        const headers = options.headers ?? new Headers();
        headers.append('Content-Type', mimeType);
        return {
            ...options,
            headers,
        };
    }

    function apiFetch(url, options = {}) {
        return fetch(url, credential(options)).then((response) => {
            return response;
        });
    }

    function getAllUser() {
        return fetch(`${DATA_URL}/users`).then((response) => response.json());
    }

    function authenticate(parametres) {
        const options = method('POST', body(parametres, mimeType('application/json')));
        return apiFetch(`${DATA_URL}/login`, options).then((response) => response.json());
    }

    return {
        getAllUser,
        authenticate,
    };
}

export default authentificationRequest;
