export function contactsRequest(JWT) {
    const DATA_URL = 'http://localhost:3000/api';

    function credential(options = {}) {
        return {
            ...options,
            credentials: 'include',
        };
    }

    function method(method, options = {}) {
        return {
            ...options,
            method,
        };
    }

    function body(data, options = {}) {
        return {
            ...options,
            body: JSON.stringify(data),
        };
    }

    function mimeType(mimeType, options = {}) {
        const headers = options.headers ?? new Headers();
        headers.append('Content-Type', mimeType);
        return {
            ...options,
            headers,
        };
    }

    function apiFetch(url, options = {}) {
        return fetch(url, credential(options)).then((response) => {
            return response;
        });
    }

    function jwt(options = {}) {
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + JWT);
        return {
            ...options,
            headers,
        };
    }

    function getAllContacts() {
        const options = method('GET', mimeType('application/json', jwt()));
        return apiFetch(`${DATA_URL}/contacts`, options);
    }

    function getContact(id) {
        const options = method('GET', mimeType('application/json', jwt()));
        return apiFetch(`${DATA_URL}/contact/${id}`, options);
    }

    function appendContact(contact) {
        const options = method('POST', body(contact, mimeType('application/json', jwt())));
        return apiFetch(`${DATA_URL}/contact`, options);
    }

    function updateContact(contact) {
        const options = method('PUT', body(contact, mimeType('application/json', jwt())));
        return apiFetch(`${DATA_URL}/contact/${contact.id}`, options);
    }

    function removeContact(contact) {
        return apiFetch(`${DATA_URL}/contact/${contact.id}`, method('DELETE', jwt()));
    }

    function register(user) {
        const options = method('POST', body(user, mimeType('application/json', jwt())));
        return apiFetch(`${DATA_URL}/user`, options).then((response) => response.json());
    }

    function getUser(id) {
        const options = method('GET', mimeType('application/json', jwt()));
        return apiFetch(`${DATA_URL}/user/${id}`, options).then((response) => response.json());
    }

    return {
        getAllContacts,
        getContact,
        appendContact,
        removeContact,
        updateContact,
        register,
        getUser
    };
}

export default contactsRequest;
