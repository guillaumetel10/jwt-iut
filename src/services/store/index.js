import React from 'react';

export const dispatchContext = React.createContext('default');

export default dispatchContext;
