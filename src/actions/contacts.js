/*
 * action types
 */

export const SET_CONTACTS = 'SET_CONTACTS';
export const UPDATE_CONTACT = 'UPDATE_CONTACT';
export const APPEND_CONTACT = 'APPEND_CONTACT';
export const REMOVE_CONTACT = 'REMOVE_CONTACT';

/*
 * action creators
 */

export function setContacts(contacts) {
    return {type: SET_CONTACTS, contacts};
}

export function updateContactReduce(contact) {
    return {type: UPDATE_CONTACT, contact};
}

export function appendContactReduce(contact) {
    return {type: APPEND_CONTACT, contact};
}

export function removeContactReduce(contact) {
    return {type: REMOVE_CONTACT, contact};
}
