/*
 * action types
 */

export const SET_JWT = 'SET_JWT';
export const SET_USER = 'SET_USER';

/*
 * action creators
 */

export function setJWT(JWT) {
    return {type: SET_JWT, JWT};
}

export function setUser(user) {
    return {type: SET_USER, user};
}
