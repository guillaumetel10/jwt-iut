import React from 'react';
import {Button, View, Image, TextInput, StyleSheet, Text} from 'react-native';
import Error from '../../Error';
import useContactUpdate from '../../../services/hooks/useContactUpdate';
import Header from '../../Header';
import {contactStyle} from '../../../styles/button';

export function ContactEdit({route}) {
    var contact =
        route.params === undefined
            ? {avatar: '', firstName: 'FirstName', lastName: 'LastName', email: 'Email', phone: 'Phone'}
            : route.params.contact;
    const {enregistrerContact} = useContactUpdate(contact);
    const [avatar, setAvatar] = React.useState(contact.avatar);
    const [firstName, setFirstName] = React.useState(contact.firstName);
    const [lastName, setLastName] = React.useState(contact.lastName);
    const [email, setEmail] = React.useState(contact.email);
    const [phone, setPhone] = React.useState(contact.phone);

    const styles = StyleSheet.create({contactStyle})

    return (
        <View>
            <Header></Header>
            <View style={{alignItems: 'center'}}>
                <View style={{...styles.contactStyle}}>
                    {
                        avatar === '' ? <Text>Image</Text>
                        :
                        <Image source={{uri: contact.avatar}} style={{width: 100, height: 100}}/>}
                    <TextInput
                        editable
                        maxLength={40}
                        value={avatar}
                        onChangeText={(avatar) => setAvatar(avatar)}
                    />
                </View>
                <View style={{...styles.contactStyle}}>
                    <Text> Prénom</Text>
                    <TextInput
                        editable
                        maxLength={40}
                        value={firstName}
                        onChangeText={(firstName) => setFirstName(firstName)}
                    />
                </View>
                <View style={{...styles.contactStyle}}>
                    <Text> Nom </Text>
                    <TextInput
                        editable
                        maxLength={40}
                        value={lastName}
                        onChangeText={(lastName) => setLastName(lastName)}
                    />
                </View>
                <View style={{...styles.contactStyle}}>
                    <Text> Email </Text>
                    <TextInput 
                        editable 
                        maxLength={40} 
                        value={email} 
                        onChangeText={(email) => setEmail(email)} 
                    />
                </View>
                <View style={{...styles.contactStyle}}>
                    <Text> Téléphone </Text>
                    <TextInput 
                        editable 
                        maxLength={40} 
                        value={phone} 
                        onChangeText={(phone) => setPhone(phone)} 
                    />
                </View>
            </View>
            <View>
                <Button
                    title="Enregistrer"
                    onPress={() => {
                        let newContact = {
                            ...contact,
                            avatar: avatar,
                            firstName: firstName,
                            lastName: lastName,
                            email: email,
                            phone: phone,
                        };
                        if (contact !== newContact) {
                            enregistrerContact(newContact);
                        }
                      }
                    }
                />
            </View>
            <Error></Error>
        </View>
    );
}

export default ContactEdit;
