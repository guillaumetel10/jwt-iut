import React, {useContext} from 'react';
import {Button, View, Text, Image, Alert, TouchableOpacity, StyleSheet, Linking, ActivityIndicator} from 'react-native';
import Error from '../Error';
import Header from '../Header';
import useContact from '../../services/hooks/useContact';
import contactsRequest from '../../services/fetch/contacts';
import useRefreshToken from '../../services/hooks/useRefreshToken';
import dispatchContext from '../../services/store';
import {removeContactReduce} from '../../actions/contacts';
import {useNavigation} from '@react-navigation/native';
import {buttonStyle, contactStyle} from '../../styles/button';

export function ContactView({route}) {
    const navigation = useNavigation();
    const {state, dispatch} = useContext(dispatchContext);
    var {contact, erreur} = useContact(route.params.contact);
    const {removeContact} = contactsRequest(state.JWT);

    const JWTExpired = useRefreshToken();

    const styles = StyleSheet.create({buttonStyle, contactStyle})
    var vue = "";
    if (contact !== undefined) {
        vue =  (
        <View>
            <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                    style={{...styles.buttonStyle, width: '50%', margin: 5}}
                    onPress={() => {
                        navigation.navigate('ContactEdit', {contact: contact});
                    }}
                ><Text>Modifier</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{...styles.buttonStyle, width: '50%', margin: 5}}
                    onPress={() => {
                        Alert.alert(
                            'Confirmation',
                            'Êtes-vous sur de vouloir supprimer ce contact ?',
                            [
                                {
                                    text: 'Cancel',
                                    onPress: () => console.log('Cancel Pressed'),
                                    style: 'cancel',
                                },
                                {
                                    text: 'OK',
                                    onPress: () => {
                                        JWTExpired(() => removeContact(contact))
                                            .then(function (data) {
                                                dispatch(removeContactReduce(contact));
                                            })
                                            .catch(function (erreur) {
                                                console.log('Erreur:', erreur);
                                            });
                                    },
                                },
                            ],
                            {cancelable: false},
                        );
                    }}
                >
                    <Text>Supprimer</Text>
                </TouchableOpacity>
            </View>
            <View style={{alignItems: 'center'}}>
                <Image source={{uri: contact.avatar}} style={{width: 100, height: 100}}/>
                <View style={{...styles.contactStyle}}>
                    <Text> Prénom</Text>
                    <Text> {contact.firstName} </Text>
                </View>
                <View style={{...styles.contactStyle}}>
                    <Text> Nom </Text>
                    <Text> {contact.lastName} </Text>
                </View>
                <View style={{...styles.contactStyle}}>
                    <Text> Email </Text>
                    <Text> {contact.email} </Text>
                </View>
                <View style={{...styles.contactStyle}}>
                    <Text> Téléphone </Text>
                    <Text> {contact.phone} </Text>
                </View>
                <TouchableOpacity
                    style={{...styles.buttonStyle, marginHorizontal: 50}}
                    onPress={() => Linking.openURL('tel:'+contact.phone)}
                    >
                    <Text>APPEL</Text>
                </TouchableOpacity>
            </View>
        </View>
        )
    } else {
        vue = <ActivityIndicator size="large" color="#00ff00" />;
    }
        return (
            <View>
                <Header/>
                {vue}
                <Error erreur={erreur}/>
            </View>
        );
}

export default ContactView;
