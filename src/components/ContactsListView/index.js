import React, {useContext} from 'react';
import {View, Text, ScrollView, TouchableOpacity, ActivityIndicator} from 'react-native';
import Error from '../Error';
import Header  from '../Header';
import ContactItem from './ContactItem';
import dispatchContext from '../../services/store';
import useContacts from '../../services/hooks/useContacts';
import {useNavigation} from '@react-navigation/native';

export function ContactsListView() {
    const navigation = useNavigation();
    const {state} = useContext(dispatchContext);
    const {erreur} = useContacts();
    var vue = '';
    if (state.contacts === undefined) {
        vue = <ActivityIndicator size="large" color="#00ff00" />;
    } else {
        vue = (
            <View>
            <TouchableOpacity style={{marginBottom:20, backgroundColor: "#DDDDDD",alignItems: "center", padding: 10}}
            onPress={() => {
                navigation.navigate('ContactEdit');
            }}
            >
                <Text>Ajouter un contact</Text>
            </TouchableOpacity>
            <ScrollView style={{marginHorizontal:20}}>
                {state.contacts.length === 0 ? (
                    <Text>Il n'y a aucun contact</Text>
                ) : (
                    state.contacts.map((contact) => <ContactItem key={contact.id} contact={contact} />)
                )}
            </ScrollView>
            </View>
        )
    }
    return (
        <View>
            <Header/>
            {vue}
            <Error erreur={erreur} />
        </View>
    );
}

export default ContactsListView;
