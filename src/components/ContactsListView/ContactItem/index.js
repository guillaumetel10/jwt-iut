import React from 'react';
import {View, Button, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faPen } from '@fortawesome/free-solid-svg-icons';

export function ContactItem(props) {
    const navigation = useNavigation();
    return (
        <View style={{backgroundColor:'lightgrey', padding: 5 , flexDirection: 'row', justifyContent:'space-around', alignItems: 'center'}}>
            <Button
                title={props.contact.firstName + ' ' + props.contact.lastName}
                onPress={() => {
                    navigation.navigate('ContactView', {contact: props.contact});
                }}
            />
            <TouchableOpacity onPress={() => {
                    navigation.navigate('ContactEdit', {contact: props.contact});
                }}>
                <FontAwesomeIcon icon={faPen} />
            </TouchableOpacity>
        </View>
    );
}

export default ContactItem;
