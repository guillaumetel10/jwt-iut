import 'react-native-gesture-handler';
import React, {useReducer} from 'react';
import AccueilView from '../AccueilView';
import RegisterView from '../RegisterView';
import ContactView from '../ContactView';
import ContactEdit from '../ContactView/ContactEdit';
import ContactsListView from '../ContactsListView';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import dispatchContext from '../../services/store';
import reducer from '../../reducer';

export function App() {
    const intitialState = {JWT: undefined, contacts: undefined, user: undefined};
    const [state, dispatch] = useReducer(reducer, intitialState);
    const Stack = createStackNavigator();

    function isLogged() {
        return state.user !== undefined;
    }

    let stackNavigator = !isLogged() ? (
        <Stack.Navigator screenOptions={() => ({headerShown: false})}>
            <Stack.Screen name="Accueil">{(props) => <AccueilView {...props} />}</Stack.Screen>
            <Stack.Screen name="RegisterView">{(props) => <RegisterView {...props} />}</Stack.Screen>
        </Stack.Navigator>) : (
        <Stack.Navigator screenOptions={() => ({headerShown: false})}>
            <Stack.Screen name="ContactsListView">{(props) => <ContactsListView {...props} />}</Stack.Screen>
            <Stack.Screen name="ContactView">{(props) => <ContactView {...props} />}</Stack.Screen>
            <Stack.Screen name="ContactEdit">{(props) => <ContactEdit {...props} />}</Stack.Screen>
        </Stack.Navigator>)
    return (
        <dispatchContext.Provider value={{state: state, dispatch: dispatch}}>
            <NavigationContainer>
                {stackNavigator}
            </NavigationContainer>
        </dispatchContext.Provider>
    );
}

export default App;
