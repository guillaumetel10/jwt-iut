import React, {useState} from 'react';
import {TouchableOpacity, TextInput, View, StyleSheet, Text} from 'react-native';
import useRegister from '../../services/hooks/useRegister';
import {inputStyle, buttonStyle, loginStyle} from '../../styles/login';

export function Register() {
    const [nom, setNom] = useState('Nom');
    let {register} = useRegister();
    const styles = StyleSheet.create({inputStyle, buttonStyle, loginStyle})

    return (
        <View style={{alignItems: 'center', height:'100%', justifyContent:'center'}}>
            <TextInput style={styles.inputStyle} value={nom} onChangeText={(text) => setLogin(text)} />
            <TouchableOpacity
                style={styles.buttonStyle}
                onPress={() => {
                    register(nom);
                }}
            >
                <Text style={styles.loginStyle}>S'enregistrer'</Text>
            </TouchableOpacity>
        </View>
    );
}

export default Register;
