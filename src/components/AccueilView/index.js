import React, {useContext, useEffect} from 'react';
import {View} from 'react-native';
import Error from '../Error';
import Login from './Login';
import dispatchContext from '../../services/store';
import {useNavigation} from '@react-navigation/native';
import useAuthentification from '../../services/hooks/useAuthentification';
import useDecodeJWT from '../../services/hooks/useDecodeJWT';
import * as Keychain from 'react-native-keychain';
import contactsRequest from '../../services/fetch/contacts';
import {setUser} from '../../actions/authentification';

export function AccueilView() {
    const navigation = useNavigation();
    const {state, dispatch} = useContext(dispatchContext);
    const {authenticate, erreur} = useAuthentification();
    const {getId} = useDecodeJWT();

    function testUser(jwt) {
        contactsRequest(jwt)
            .getUser(getId(jwt))
                .then((data)=> {
                    if (data.code === "notfound") {
                        navigation.navigate('RegisterView');
                    } else {
                        dispatch(setUser(data.name));
                    }
                })
                .catch((erreur) => {
                    console.log(erreur);
                });
    }  

    useEffect(() => {
        if (state.JWT === undefined ) {
            (async () => {
                try {
                    const credentials = await Keychain.getGenericPassword();
                    if (credentials.password) {
                        authenticate({login: credentials.username, refreshToken: credentials.password}).then((jwt) => {
                            testUser(jwt);
                            }
                        )
                    }
                } catch (error) {
                    console.log('Aucun Refresh Token', error);
                }
            })();
        }
    }, [state.JWT]);
    
    return (
        <View style={{height:'100%', justifyContent:'center'}}>
            <Login testUser={testUser} />
            <Error erreur={erreur}/>
        </View>
    );
}

export default AccueilView;
