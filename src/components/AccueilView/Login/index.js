import React, {useState, useContext} from 'react';
import {Button, TextInput, View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import {useAuthentification} from '../../../services/hooks/useAuthentification';
import {useRegister} from '../../../services/hooks/useRegister';
import {inputStyle, buttonStyle, loginStyle} from '../../../styles/login';
import dispatchContext from '../../../services/store';

export function Login(props) {
    const [login, setLogin] = useState('Login');
    const [password, setPassword] = useState('Password');
    const {state} = useContext(dispatchContext);
    let {authenticate} = useAuthentification();

    const styles = StyleSheet.create({inputStyle, buttonStyle, loginStyle})
    return (
        <View style={{alignItems: 'center'}}>
            <TextInput style={styles.inputStyle} value={login} onChangeText={(text) => setLogin(text)} />
            <TextInput style={styles.inputStyle} value={password} onChangeText={(text) => setPassword(text)} />
            <TouchableOpacity
                style={styles.buttonStyle}
                onPress={() => {
                    authenticate({login: login, password: password});
                    props.testUser(state.JWT);
                }}
            >
                <Text style={styles.loginStyle}>Se connecter</Text>
            </TouchableOpacity>
        </View>
    );
}

export default Login;
