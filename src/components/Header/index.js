import React, {useContext} from 'react';
import { Button, View, Text} from "react-native";
import dispatchContext from '../../services/store';
import useAuthentification from '../../services/hooks/useAuthentification';
import {useNavigation} from '@react-navigation/native';

export function Header() {
    const navigation = useNavigation();
    const {state, dispatch} = useContext(dispatchContext);
    const {disconnect} = useAuthentification();
    return (
        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom:5}}>
            <View style={{width: '33%'}}>
            {navigation.canGoBack() ? <Button title="Back" onPress={navigation.goBack}></Button> : null}
            </View>
            <Text>{state.user}</Text>
            <Button title="Déconnexion" onPress={disconnect}></Button>
        </View>
    );
}

export default Header;