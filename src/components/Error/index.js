import React from 'react';
import {View, Text} from 'react-native';

export function Error(props) {
    return (
        <View>
            <Text>{props.erreur}</Text>
        </View>
    );
}

export default Error;
