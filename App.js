import React from 'react';
import {App as Appl} from './src/components/App';

const App: () => React$Node = () => {
    return (
        <Appl/>
    );
};

export default App;
